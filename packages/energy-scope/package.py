# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install energy-scope
#
# You can edit this file again by typing:
#
#     spack edit energy-scope
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *
#import os

def es_date2version(es_dist_file):
    fstep = es_dist_file.split('_')
    if 'energy-scope' in es_dist_file:
        es_date=fstep[1][1:]
    else: #suppose energy_scope
        es_date=fstep[2][1:]
    es_year=es_date.split('-')[0]
    es_majeur = str(int(es_year) - 2021)
    es_month=es_date.split('-')[1]
    es_minor = str(int(es_month))
    es_third = str(int(es_date.split('-')[2]))
    return es_majeur + '.' + es_minor +'.' + es_third
    
class EnergyScope(Package):
    """energy scope is a tool to get energy profiles of large applications on HPC"""
    extends('python') 

    homepage = 'https://sed-bso.gitlabpages.inria.fr/datacenter/energy_scope.html'
    url      = ''

    es_dist_file='energy-scope_v2022-04-05_acquisition.tar'
    es_dist_version=es_date2version(es_dist_file)
    version(es_dist_version,
            '93656b0de6b682e40006ffa529f6b1eb30d08759da59d23e7234a1cda3a00259',
            url='https://sed-bso.gitlabpages.inria.fr/datacenter/' + es_dist_file, preferred=True)

    es_dist_file='energy-scope_v2022-02-24_acquisition.tar'
    es_dist_version=es_date2version(es_dist_file)
    version(es_dist_version,
            '15589bd9ff9d0083499fa9115c4aad838155120f85888c63fc8fcab87d98a57b',
            url='https://sed-bso.gitlabpages.inria.fr/datacenter/' + es_dist_file)

    es_dist_file='energy_scope_v2022-02-18_acquisition.tar'
    es_dist_version=es_date2version(es_dist_file)
    version(es_dist_version,
            '9223bfab9eab2ca6c15d42b1d53821da0de3ce9ff9f73015492cd5ef84396fe6',
            url='https://sed-bso.gitlabpages.inria.fr/datacenter/' + es_dist_file)

    def install(self, spec, prefix):
        install_tree('./', prefix)
        return
    def setup_run_environment(self, env):
        env.set('ENERGY_SCOPE_SRC_DIR', self.prefix)
        env.set('ENERGY_SCOPE_TIMEOUT', '0')
        env.set('ENERGY_SCOPE_PYTHON', 'python3')
        #env.set('ENERGY_SCOPE_USER_NAME_PREFIX', os.environ['USER'])
        env.prepend_path('PATH', self.prefix)
        
