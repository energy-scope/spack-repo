# energy scope spack-repo

spack repo for energy scope distribution via spack

## Introduction

### Spack 

Spack is a package management tool :

* spack repositiory: https://github.com/LLNL/spack
* spack documentation:  http://spack.readthedocs.io/en/latest

### Energy Scope

Energy Scope is a software package to do energy measurements and to identify energy profile of HPC codes.

* Web Site: https://sed-bso.gitlabpages.inria.fr/datacenter/energy_scope.html

## installation

### Spack 

The first step consist in cloning the https://github.com/LLNL/spack. The baseline instruction shall ideally be: 
~~~
git clone https://github.com/llnl/spack.git
~~~

Shell support
~~~
# For bash/zsh/sh
$ . spack/share/spack/setup-env.sh
~~~

More information: https://spack.readthedocs.io/en/latest/getting_started.html


## Add up the present repository to extend reference spack packages with the energy-scope package

Once the reference spack is fully installed, the second consists in adding up the present repository to the list of available packages.

For that, this repository shall be pulled into a local directory. Assuming that the directory path is stored in the environment variable INSTALL_DIR, this can be done with the following instruction: 
~~~
git clone git@gitlab.inria.fr:energy-scope/spack-repo.git $INSTALL_DIR
~~~

We can now use the spack repo command to create a local package repository:
~~~
spack repo add $INSTALL_DIR
~~~

Note: If you have not fully configured spack following the detailed reference instructions here, you must provide the spack absolute path
~~~
./spack/bin/spack repo add $INSTALL_DIR
~~~

### Test

You should have access to the energy-scope package with your spack. To test:
~~~
spack spec energy-scope
~~~

### Install/uninstall/Update packages

~~~
spack install energy-scope
spack uninstall energy-scope
~~~

To update the recipes use git pull in the spack repository

### Setup environment

If you have shell support enabled you can use the spack load command to quickly get a package on your PATH, etc
~~~
spack install energy-scope
spack load energy-scope
~~~


